<?PHP

$url = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?view=label&item_barcode=".$_GET['barcode']."&apikey=XXXXXXXXXXXXX";
$curl = curl_init( $url );
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
$result = curl_exec($curl);
if ($result === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
} else {
}
curl_close();

$xml = simplexml_load_string($result);
print '{"title": "'.addcslashes($xml->bib_data->title, "\"").'", "author": "'.addcslashes($xml->bib_data->author, "\"").'", "callno": "'.addcslashes($xml->holding_data->call_number, "\"").'", "description": "'.addcslashes($xml->item_data->description, "\"").'", "barcode": "'.$xml->item_data->barcode.'", "copy": "'.$xml->holding_data->copy_id.'", "parsed_callno": '.json_encode($xml->item_data->parsed_call_number).', "library": "'.$xml->item_data->library.'"}';
?>
